export default {
  isLoading: (state) => state.loading,
  hasError: (state) => state.error,
  getUserList: (state) => state.data.userList,
  getUserData: (state) => state.data.userData,
  getUserById: (state) => (userId) => {
    const { userList } = state.data;
    return userList.find((u) => u.id === userId);
  },
};
