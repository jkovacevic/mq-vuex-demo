import MT from './types';

function setLoading(state) {
  state.loading = true;
}

function setError(state, payload) {
  state.loading = false;
  state.error = payload;
}

function setUserList(state, payload) {
  state.data.userList = payload;
  state.loading = false;
  state.error = null;
}

function setUserData(state, payload) {
  state.data.userData = payload;
  state.loading = false;
  state.error = null;
}

function updateUser(state, payload) {
  const { id } = payload;
  const userIndex = state.data.userList.findIndex((u) => u.id === id);
  if (userIndex > -1) state.data.userList[userIndex] = payload;

  state.loading = false;
  state.error = null;
}

function removeUser(state, userId) {
  const userIndex = state.data.userList.findIndex((u) => u.id === userId);
  if (userIndex > -1) state.data.userList.splice(userIndex, 1);

  state.loading = false;
  state.error = null;
}

export default {
  [MT.LOADING]: setLoading,
  [MT.ERROR]: setError,
  [MT.SET_USER_LIST]: setUserList,
  [MT.SET_USER_DATA]: setUserData,
  [MT.UPDATE_USER]: updateUser,
  [MT.REMOVE_USER]: removeUser,
};
