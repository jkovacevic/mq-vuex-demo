export default {
  loading: false,
  error: null,
  data: {
    userList: [],
    userData: {
      name: '',
      email: '',
      birthday: '',
      country: '',
      phone: '',
    },
  },
};
