import MT from './types';
import http from '@/http/fetchApi';

export default {
  async createUser({ commit }, userData) {
    try {
      commit(MT.LOADING);
      await http.post('api/users', { data: { ...userData } });
      commit(MT.SET_USER_DATA, userData);
    } catch (err) {
      commit(MT.ERROR, err);
    }
  },
  async fetchUserList({ commit }) {
    try {
      commit(MT.LOADING);
      const { data } = await http.get('api/users');
      commit(MT.SET_USER_LIST, data);
    } catch (err) {
      commit(MT.ERROR, err);
    }
  },
  async fetchUserData({ commit }, userId) {
    try {
      commit(MT.LOADING);
      const { data } = await http.get(`api/users/${userId}`);
      commit(MT.SET_USER_DATA, data);
    } catch (err) {
      commit(MT.ERROR, err);
    }
  },
  async updateUser({ commit }, payload) {
    try {
      commit(MT.LOADING);
      await http.patch(`api/users/${payload.id}`, {
        data: { ...payload },
      });
      commit(MT.UPDATE_USER, payload);
    } catch (err) {
      commit(MT.ERROR, err);
    }
  },
  async removeUser({ commit }, userId) {
    try {
      commit(MT.LOADING);
      await http.delete(`api/users/${userId}`);
      commit(MT.REMOVE_USER, userId);
    } catch (err) {
      commit(MT.ERROR, err);
    }
  },
  // async removeUser({ commit, state }, userId) {
  //   const user = state.data.userList.find((u) => u.id === userId);
  //   try {
  //     commit(MT.REMOVE_USER, userId);
  //     await http.delete(`api/users/${userId}`);
  //     commit(MT.REMOVE_USER_SUCCESS);
  //   } catch (err) {
  //     commit(MT.REMOVE_USER_ERROR, user);
  //   }
  // },
};
