import MT from './types';

function setConnection(state, connection) {
  state.connection = connection;
}

function updateConnectionMessages(state, message) {
  state.connectionMessages.push(message);
}

export default {
  [MT.SET_CONNECTION]: setConnection,
  [MT.UPDATE_CONNECTION_MESSAGES]: updateConnectionMessages,
};
