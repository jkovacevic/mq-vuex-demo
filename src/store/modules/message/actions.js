import MT from './types';
import ws from '@/http/ws';

export default {
  connect({ commit, dispatch }, url) {
    const connection = ws.connect(url);
    commit(MT.SET_CONNECTION, connection);

    connection.onopen = (e) => {
      console.log('[onopen]: ', e);
    };

    connection.onmessage = (e) => {
      console.log('[onmessasge] data: ', e.data);
      dispatch('updateConnectionMessages', e.data);
    };

    connection.onerror = (e) => {
      console.error('[onerror]: ', e);
    };

    connection.onclose = (e) => {
      console.log('[onclose]: ', e);
    };
  },
  updateConnectionMessages({ commit }, msg) {
    commit(MT.UPDATE_CONNECTION_MESSAGES, msg);
  },
};
