export default {
  SET_CONNECTION: 'SET_CONNECTION',
  UPDATE_CONNECTION_MESSAGES: 'UPDATE_CONNECTION_MESSAGES',
};
