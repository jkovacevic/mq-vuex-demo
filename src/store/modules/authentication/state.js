export default {
  loading: false,
  error: null,
  data: {
    username: '',
    token: '',
    expiresAt: null,
  },
};
