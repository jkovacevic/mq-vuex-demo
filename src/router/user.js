export default [
  {
    path: '/user',
    component: () => import('@/views/user/Index.vue'),
    meta: { authenticate: true },
    children: [
      {
        path: '',
        name: 'list',
        component: () => import('@/views/user/List.vue'),
        meta: { authenticate: true },
      },
      {
        path: ':id',
        name: 'view',
        component: () => import('@/views/user/View.vue'),
        meta: { authenticate: true },
      },
      {
        path: 'edit/:id',
        name: 'edit',
        component: () => import('@/views/user/Edit.vue'),
        meta: { authenticate: true },
      },
      {
        path: 'create',
        name: 'create',
        component: () => import('@/views/user/Create.vue'),
        meta: { authenticate: true },
      },
    ],
  },
];
