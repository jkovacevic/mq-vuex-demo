export default (to, from, next) => {
  const authenticationRequired = to.meta.authenticate;
  const isAuthenticated = sessionStorage.getItem('auth');
  if (authenticationRequired && isAuthenticated) {
    return next();
  }

  if (!authenticationRequired) {
    return next();
  }

  return next('/login');
};
