import baseHttp from './baseFetch';

export default {
  get: (url, params, options) => baseHttp(url, 'get', {
    ...options,
    params,
  }),
  post: (url, data, options) => baseHttp(url, 'post', {
    data,
    ...options,
  }),
  update: (url, data, options) => baseHttp(url, 'update', {
    data,
    ...options,
  }),
  put: (url, data, options) => baseHttp(url, 'put', {
    data,
    ...options,
  }),
  patch: (url, data, options) => baseHttp(url, 'patch', {
    data,
    ...options,
  }),
  delete: (url, data, options) => baseHttp(url, 'delete', {
    data,
    ...options,
  }),
};
