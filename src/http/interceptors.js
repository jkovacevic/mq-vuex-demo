import Swal from 'sweetalert2';

const postMethods = ['post', 'patch'];

export function requestInterceptor(config) {
  const isMethodIncluded = postMethods.includes(config.method.toLowerCase());
  const hasId = Object.prototype.hasOwnProperty.call('id', config?.data?.data);
  if (isMethodIncluded && hasId && !config.data.data.id) {
    throw new Error('You need to pass an ID for this request');
  }

  return config;
}

export function responseInterceptor(response) {
  return response;
}

export function errorInterceptor(error) {
  Swal.fire({
    icon: 'error',
    title: 'Error!',
    text: error.message,
  });

  return Promise.reject(error);
}
