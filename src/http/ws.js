let connection;

export default {
  connect(url) {
    connection = new WebSocket(`wss://${url}`);
    return connection;
  },
};
