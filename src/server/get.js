export default {
  from: (key) => (schema) => schema.db[key],
  fromBy: (key) => (schema, request) => {
    const id = +request.params.id;
    return schema.db[key].find(id);
  },
};
