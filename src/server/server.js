import { Server } from 'miragejs';
import baseData from './db';
import getApi from './get';
import postApi from './post';
import patchApi from './patch';
import deleteApi from './delete';

window.server = new Server({
  seeds(srv) {
    srv.db.loadData({ ...baseData });
  },
  routes() {
    this.namespace = 'api';
    this.timing = 100;

    this.get('/users', getApi.from('users'));
    this.get('/users/:id', getApi.fromBy('users'));
    this.post('/users', postApi.from('users'));
    this.patch('/users/:id', patchApi.from('users'));
    this.delete('/users/:id', deleteApi.from('users'));
  },
});
