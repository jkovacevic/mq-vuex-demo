export default {
  from: (key) => (schema, request) => {
    const id = +request.params.id;
    return schema.db[key].remove(id);
  },
};
